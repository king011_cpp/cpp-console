#!/usr/bin/env bash
set -e

BashDir=$(cd "$(dirname $BASH_SOURCE)" && pwd)
eval $(cat "$BashDir/conf.sh")
if [[ "$Command" == "" ]];then
    Command="$0"
fi
filename="$Dir/include/version.h"
function write(){
    echo "#ifndef __${Macro}__VERSION_H__" > "$filename"
    echo  "#define __${Macro}__VERSION_H__" >> "$filename"
    echo >> "$filename"
    echo  "#define ${Macro}_VERSION \"v0.0.1\"" >> "$filename"
    echo >> "$filename"
    echo  "#endif // __${Macro}__VERSION_H__" >> "$filename"
}
if [[ -f "$filename" ]];then
    version=$(grep Version "$filename" | awk '{print $3}')
    if [[ "\`$Version\`" !=  "$version" ]];then
        write
    fi
else
    write
fi